require.config({
    paths: {
      jquery: '../node_modules/jquery/dist/jquery',
      underscore: '../node_modules/underscore/underscore',
      backbone: '../node_modules/backbone/backbone',
      handlebars: '../node_modules/handlebars/dist/handlebars',
      text: '../node_modules/requirejs-text/text'
    }
  });
  
  require([
    'app',
    'text'
  ], function(App){
    App.initialize();
  });