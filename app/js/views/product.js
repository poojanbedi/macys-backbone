define([
    'jquery',
    'handlebars',
    'backbone',
    // Using the Require.js text! plugin, we are loaded raw text
    // which will be used as our views primary template
    'text!/templates/product.html'
  ], function($, handlebars, Backbone, productTmpl){
    let ProjectListView = Backbone.View.extend({
      el: $('#container'),
      render: function(){
        
        let data = {};
        let compiledTemplate = handlebars.compile(productTmpl);
        
        this.$el.append( compiledTemplate(this) );
      },
      sizes: [ {code:'s', name:'Small'}, {code:'m', name:'Medium'}, {code:'l', name: 'Large'} ],
      colors: [ {code:'red', name:'Red'}, {code:'green', name:'Green'},{code:'blue', name:'Blue'}],
      qty: 1
    });
    // Our module now returns our view
    return ProjectListView;
  });