define([
    'jquery',
    'handlebars',
    'backbone',
    'js/router.js',
  ], function($, handlebars, Backbone, Router){
    var initialize = function(){
      // Pass in our Router module and call it's initialize function
      Router.initialize();
    }
  
    return {
      initialize: initialize
    };
  });