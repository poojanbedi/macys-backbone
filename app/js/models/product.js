define([
    'backbone'
  ], function(Backbone){
    var ProjectModel = Backbone.Model.extend({
      defaults: {
        size: 'xl',
        color: 'red',
        qty: 1
      }
    });
    // Return the model for the module
    return ProjectModel;
  });