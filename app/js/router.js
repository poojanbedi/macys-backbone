define([
    'jquery',
    'handlebars',
    'backbone',
    'views/product',
    // 'views/users/list'
  ], function($, handlebars, Backbone, ProductView){
    var AppRouter = Backbone.Router.extend({
      routes: {
        // Define some URL routes
        'product': 'showProductView',
        'shipping': 'shipping',
        '*path':  'defaultRoute'
      }
    });
  
    var initialize = function(){
      var app_router = new AppRouter;
      var routeFunction = {
          showProductView() {
            var productView = new ProductView();
            productView.render();
          },
          shippingRoute() {

          },
          defaultRoute() {
            routeFunction.showProductView();
          }
        };
      app_router.on('route:showProductView', routeFunction.showProductView);
      app_router.on('route:shipping', routeFunction.shippingRoute);
      app_router.on('route:defaultRoute', routeFunction.defaultRoute);
      Backbone.history.start();
    };
    return {
      initialize: initialize
    };
  });